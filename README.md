# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## Scaling the Cuisine Registry
So, obviously, we want the registry to be its own, standalone, service which can be deployed in multiple instances and
provide REST api access to its functionality.
The in-memory storage we currently have will no longer hold water, since currently there is n mechanism of distribution.
There are different ways of dealing with this, e.g. routing all requests to the cuisine registry service to their relevant node,
but I would consider using already existing tools for distributed, in-memory storage, such as Hazelcast.
Calculating top N cuisines is currently an operation that could easily get expensive - so a counter per cuisine type should
be maintained.  If eventual consistency is acceptable, I would emit a message to kafka (or other), upon each successful call
to the register api, and handle the counters asynchronously.