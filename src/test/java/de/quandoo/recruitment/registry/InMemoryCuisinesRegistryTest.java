package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void shouldRegisterCustomerCuisines() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldReturnItalianCuisineLovers() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));

        List<Customer> italianLovers = cuisinesRegistry.cuisineCustomers(new Cuisine("italian"));

        assertNotNull(italianLovers);
        assertEquals(2, italianLovers.size());
    }

    @Test
    public void shouldReturnCustomerCuisines() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("indian"));

        List<Cuisine> user2Cuisines = cuisinesRegistry.customerCuisines(new Customer("2"));

        assertNotNull(user2Cuisines);
        assertEquals(2, user2Cuisines.size());
        assertTrue(user2Cuisines.contains(new Cuisine("italian")));
        assertTrue(user2Cuisines.contains(new Cuisine("indian")));
    }

    @Test
    public void shouldReturnEmptyCustomerListForNullCuisine() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
        assertNotNull(customers);
        assertEquals(0, customers.size());
    }

    @Test
    public void shouldReturnEmptyCuisineListForNullCustomer() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(null);
        assertNotNull(cuisines);
        assertEquals(0, cuisines.size());
    }

    @Test
    public void shouldReturnTop1Cuisine() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);

        assertNotNull(topCuisines);
        assertEquals(1, topCuisines.size());
        assertEquals("french", topCuisines.get(0).getName());
    }


}