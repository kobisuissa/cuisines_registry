package de.quandoo.recruitment.registry.model;

public class Customer {
    private final String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else {
            if (Customer.class.isAssignableFrom(obj.getClass())) {
                return getUuid().equals(((Customer) obj).getUuid());
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return getUuid().hashCode();
    }
}
