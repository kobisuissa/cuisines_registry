package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else {
            if (Cuisine.class.isAssignableFrom(obj.getClass())) {
                return getName().equals(((Cuisine) obj).getName());
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
