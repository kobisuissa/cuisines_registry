package de.quandoo.recruitment.registry;

import com.google.common.collect.*;
import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    // A synchronized access multimap for storing cuisine customers
    private final Multimap<Cuisine, Customer> cuisineCustomers = Multimaps.synchronizedMultimap(HashMultimap.create());

    // A synchronized access multimap for storing customer cuisines
    // NOTE: this multimap is maintained for the sake of O(1) access to a customer's cuisine list
    private final Multimap<Customer, Cuisine> customerCuisines = Multimaps.synchronizedMultimap(HashMultimap.create());

    @Override
    /**
     * Registers a customer cuisine preference.
     *
     * Creates one entry in the <i>cuisineCustomers</i> Multimap, as well as one entry in the
     * <i>customerCuisines</i> Multimap.
     *
     * Accepts all types of cuisines.
     */
    public void register(final Customer userId, final Cuisine cuisine) {
        cuisineCustomers.put(cuisine, userId);
        customerCuisines.put(userId, cuisine);
    }

    @Override
    /**
     * return a list of customer who have added the given cuisine to their favorites
     */
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return ImmutableList.copyOf(cuisineCustomers.get(cuisine));
    }

    @Override
    /**
     * returns a list of cuisines added as favorites by the given customer
     */
    public List<Cuisine> customerCuisines(final Customer customer) {
        return ImmutableList.copyOf(customerCuisines.get(customer));
    }

    @Override
    /**
     * returns the top n cuisines, i.e. the n cuisines that have been added as favorites by most customers
     */
    public List<Cuisine> topCuisines(final int n) {
        //As this method access a map view of the synchronized multimap, which is NOT synchronized, we must handle it.
        synchronized (cuisineCustomers) {
            List<Cuisine> topCuisines = cuisineCustomers
                    .asMap()
                    .entrySet()
                    .stream()
                    .sorted((o1, o2) -> {
                        int o1Size = o1.getValue().size();
                        int o2Size = o2.getValue().size();
                        return o1Size == o2Size ? 0 : (o1Size > o2Size ? -1 : 1);
                    })
                    .limit(n)
                    .map(entry -> entry.getKey())
                    .collect(Collectors.toList());

            return topCuisines;
        }
    }
}
